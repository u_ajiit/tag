
.phony=all


all:

	sudo apt-get install exuberant-ctags
	sudo apt-get install cscope
	mkdir -p ~/.vim/plugins
	cd ~/.vim/plugins
	wget http://cscope.sourceforge.net/cscope_maps.vim
	cd -
	chmod +x gentags.sh
	echo "source ~/.vim/plugins/cscope_maps.vim" >> ~/.vimrc
	sudo ./gentags 
	sudo source ~/.vimrc 
